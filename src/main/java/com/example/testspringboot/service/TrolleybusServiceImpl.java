package com.example.testspringboot.service;

import com.example.testspringboot.model.Trolleybus;
import com.example.testspringboot.repository.TrolleybusRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TrolleybusServiceImpl implements TrolleybusService {

    @Autowired
    private TrolleybusRepository trolleybusRepository;

    @Override
    public void create(Trolleybus trolleybus) {
        trolleybusRepository.save(trolleybus);
    }

    @Override
    public List<Trolleybus> readAll() {
        return trolleybusRepository.findAll();
    }

    @Override
    public Trolleybus read(int id) {
        return trolleybusRepository.getById(id);
    }

    @Override
    public boolean update(Trolleybus trolleybus, int id) {
        if (trolleybusRepository.existsById(id)) {
            trolleybus.setId(id);
            trolleybusRepository.save(trolleybus);
            return true;
        }
        return false;
    }

    @Override
    public boolean delete(int id) {
        if (trolleybusRepository.existsById(id)) {
            trolleybusRepository.deleteById(id);
            return true;
        }
        return false;
    }
}