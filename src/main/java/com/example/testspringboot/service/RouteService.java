package com.example.testspringboot.service;

import com.example.testspringboot.model.Route;

import java.util.List;

public interface RouteService {

    void create(Route route);

    List<Route> readAll();

    Route read(int Id);

    boolean update(Route Route, int id);

    boolean delete(int id);
}
