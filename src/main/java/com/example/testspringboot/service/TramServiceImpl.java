package com.example.testspringboot.service;


import com.example.testspringboot.model.Tram;
import com.example.testspringboot.repository.TramRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TramServiceImpl implements TramService {

    @Autowired
    private TramRepository tramRepository;

    @Override
    public void create(Tram tram) {
        tramRepository.save(tram);
    }

    @Override
    public List<Tram> readAll() {
        return tramRepository.findAll();
    }

    @Override
    public Tram read(int id) {
        return tramRepository.getById(id);
    }

    @Override
    public boolean update(Tram tram, int id) {
        if (tramRepository.existsById(id)) {
            tram.setId(id);
            tramRepository.save(tram);
            return true;
        }
        return false;
    }

    @Override
    public boolean delete(int id) {
        if (tramRepository.existsById(id)) {
            tramRepository.deleteById(id);
            return true;
        }
        return false;
    }


}