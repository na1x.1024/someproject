package com.example.testspringboot.service;

import com.example.testspringboot.model.Bus;

import java.util.List;

public interface BusService {

    void create(Bus bus);

    List<Bus> readAll();

    Bus read(int id);

    boolean update(Bus Bus, int id);

    boolean delete(int id);
}
