package com.example.testspringboot.service;

import com.example.testspringboot.model.Trolleybus;

import java.util.List;

public interface TrolleybusService {

    void create(Trolleybus employee);

    List<Trolleybus> readAll();

    Trolleybus read(int id);

    boolean update(Trolleybus employee, int id);

    boolean delete(int id);
}