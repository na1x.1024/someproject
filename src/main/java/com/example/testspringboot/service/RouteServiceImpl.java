package com.example.testspringboot.service;


import com.example.testspringboot.model.Route;
import com.example.testspringboot.repository.RouteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RouteServiceImpl implements RouteService {

    @Autowired
    private RouteRepository routeRepository;

    @Override
    public void create(Route route) {
        routeRepository.save(route);
    }

    @Override
    public List<Route> readAll() {
        return routeRepository.findAll();
    }

    @Override
    public Route read(int id) {
        return routeRepository.getById(id);
    }

    @Override
    public boolean update(Route route, int id) {
        if (routeRepository.existsById(id)) {
            route.setId(id);
            routeRepository.save(route);
            return true;
        }
        return false;
    }

    @Override
    public boolean delete(int id) {
        if (routeRepository.existsById(id)) {
            routeRepository.deleteById(id);
            return true;
        }
        return false;
    }


}