package com.example.testspringboot.service;

import com.example.testspringboot.model.Tram;

import java.util.List;

public interface TramService {

    void create(Tram tram);

    List<Tram> readAll();

    Tram read(int Id);

    boolean update(Tram Tram, int id);

    boolean delete(int id);
}
