package com.example.testspringboot.service;


import com.example.testspringboot.model.Bus;
import com.example.testspringboot.repository.BusRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BusServiceImpl implements BusService {

    @Autowired
    private BusRepository busRepository;

    @Override
    public void create(Bus bus) {
        busRepository.save(bus);
    }

    @Override
    public List<Bus> readAll() {
        return busRepository.findAll();
    }

    @Override
    public Bus read(int id) {
        return busRepository.getById(id);
    }

    @Override
    public boolean update(Bus bus, int id) {
        if (busRepository.existsById(id)) {
            bus.setId(id);
            busRepository.save(bus);
            return true;
        }
        return false;
    }

    @Override
    public boolean delete(int id) {
        if (busRepository.existsById(id)) {
            busRepository.deleteById(id);
            return true;
        }
        return false;
    }


}