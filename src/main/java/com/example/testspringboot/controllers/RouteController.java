package com.example.testspringboot.controllers;



import com.example.testspringboot.model.Route;
import com.example.testspringboot.service.RouteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class RouteController {

    private final RouteService routeService;

    @Autowired
    public RouteController(RouteService routeService) {
        this.routeService = routeService;
    }

    @PostMapping(value = "/routes")
    public ResponseEntity<?> create(@RequestBody Route route) {
        routeService.create(route);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @GetMapping(value = "/routes")
    public ResponseEntity<List<Route>> read() {
        final List<Route> routes = routeService.readAll();
        return routes != null && !routes.isEmpty()
                ? new ResponseEntity<>(routes, HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @GetMapping(value = "/routes/byUserId/{id}")
    public ResponseEntity<Route> read(@PathVariable(name = "id") int id) {
        final Route route = routeService.read(id);
        return route != null
                ? new ResponseEntity<>(route, HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @PutMapping(value = "/routes/byId/{id}")
    public ResponseEntity<?> update(@PathVariable(name = "id") int id, @RequestBody Route route) {
        final boolean updated = routeService.update(route, id);

        return updated
                ? new ResponseEntity<>(HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NOT_MODIFIED);
    }

    @DeleteMapping(value = "/routes/byId/{id}")
    public ResponseEntity<?> delete(@PathVariable(name = "id") int id) {
        final boolean deleted = routeService.delete(id);

        return deleted
                ? new ResponseEntity<>(HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NOT_MODIFIED);
    }

}