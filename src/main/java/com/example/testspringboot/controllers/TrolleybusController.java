package com.example.testspringboot.controllers;


import com.example.testspringboot.model.Trolleybus;
import com.example.testspringboot.service.TrolleybusService;
import com.example.testspringboot.service.TrolleybusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class TrolleybusController {

    private final TrolleybusService trolleybusService;

    @Autowired
    public TrolleybusController(TrolleybusService trolleybusService) {
        this.trolleybusService = trolleybusService;
    }

    @PostMapping(value = "/trolleybuses")
    public ResponseEntity<?> create(@RequestBody Trolleybus trolleybus) {
        trolleybusService.create(trolleybus);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @GetMapping(value = "/trolleybuses")
    public ResponseEntity<List<Trolleybus>> read() {
        final List<Trolleybus> trolleybuses = trolleybusService.readAll();
        return trolleybuses != null && !trolleybuses.isEmpty()
                ? new ResponseEntity<>(trolleybuses, HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @GetMapping(value = "/trolleybuses/byId/{id}")
    public ResponseEntity<Trolleybus> read(@PathVariable(name = "id") int id) {
        final Trolleybus trolleybus = trolleybusService.read(id);
        return trolleybus != null
                ? new ResponseEntity<>(trolleybus, HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @PutMapping(value = "/trolleybuses/byId/{id}")
    public ResponseEntity<?> update(@PathVariable(name = "id") int id, @RequestBody Trolleybus trolleybus) {
        final boolean updated = trolleybusService.update(trolleybus, id);

        return updated
                ? new ResponseEntity<>(HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NOT_MODIFIED);
    }

    @DeleteMapping(value = "/trolleybuses/byId/{id}")
    public ResponseEntity<?> delete(@PathVariable(name = "id") int id) {
        final boolean deleted = trolleybusService.delete(id);

        return deleted
                ? new ResponseEntity<>(HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NOT_MODIFIED);
    }

}