package com.example.testspringboot.controllers;


import com.example.testspringboot.model.Bus;
import com.example.testspringboot.service.BusService;
import com.example.testspringboot.service.BusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class BusController {

    private final BusService busService;

    @Autowired
    public BusController(BusService busService) {
        this.busService = busService;
    }

    @PostMapping(value = "/buses")
    public ResponseEntity<?> create(@RequestBody Bus bus) {
        busService.create(bus);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @GetMapping(value = "/buses")
    public ResponseEntity<List<Bus>> read() {
        final List<Bus> buses = busService.readAll();
        return buses != null && !buses.isEmpty()
                ? new ResponseEntity<>(buses, HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @GetMapping(value = "/buses/byId/{id}")
    public ResponseEntity<Bus> read(@PathVariable(name = "id") int id) {
        final Bus bus = busService.read(id);
        return bus != null
                ? new ResponseEntity<>(bus, HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @PutMapping(value = "/buses/byId/{id}")
    public ResponseEntity<?> update(@PathVariable(name = "id") int id, @RequestBody Bus bus) {
        final boolean updated = busService.update(bus, id);

        return updated
                ? new ResponseEntity<>(HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NOT_MODIFIED);
    }

    @DeleteMapping(value = "/buses/byId/{id}")
    public ResponseEntity<?> delete(@PathVariable(name = "id") int id) {
        final boolean deleted = busService.delete(id);

        return deleted
                ? new ResponseEntity<>(HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NOT_MODIFIED);
    }

}