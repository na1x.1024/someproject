package com.example.testspringboot.controllers;



import com.example.testspringboot.model.Tram;
import com.example.testspringboot.service.TramService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class TramController {

    private final TramService tramService;

    @Autowired
    public TramController(TramService tramService) {
        this.tramService = tramService;
    }

    @PostMapping(value = "/trams")
    public ResponseEntity<?> create(@RequestBody Tram tram) {
        tramService.create(tram);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @GetMapping(value = "/trams")
    public ResponseEntity<List<Tram>> read() {
        final List<Tram> trams = tramService.readAll();
        return trams != null && !trams.isEmpty()
                ? new ResponseEntity<>(trams, HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @GetMapping(value = "/trams/byUserId/{id}")
    public ResponseEntity<Tram> read(@PathVariable(name = "id") int id) {
        final Tram tram = tramService.read(id);
        return tram != null
                ? new ResponseEntity<>(tram, HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @PutMapping(value = "/trams/byId/{id}")
    public ResponseEntity<?> update(@PathVariable(name = "id") int id, @RequestBody Tram tram) {
        final boolean updated = tramService.update(tram, id);

        return updated
                ? new ResponseEntity<>(HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NOT_MODIFIED);
    }

    @DeleteMapping(value = "/trams/byId/{id}")
    public ResponseEntity<?> delete(@PathVariable(name = "id") int id) {
        final boolean deleted = tramService.delete(id);

        return deleted
                ? new ResponseEntity<>(HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NOT_MODIFIED);
    }

}