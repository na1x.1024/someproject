package com.example.testspringboot.repository;

import com.example.testspringboot.model.Tram;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TramRepository extends JpaRepository<Tram, Integer> {

}
