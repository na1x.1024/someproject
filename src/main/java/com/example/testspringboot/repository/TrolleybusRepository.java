package com.example.testspringboot.repository;

import com.example.testspringboot.model.Trolleybus;
import org.springframework.data.jpa.repository.JpaRepository;


public interface TrolleybusRepository extends JpaRepository<Trolleybus, Integer> {



}
