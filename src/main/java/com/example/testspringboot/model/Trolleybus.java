package com.example.testspringboot.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

@Entity
@Table(name = "trolleybuses")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Trolleybus {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "trolleybus_number")
    private Integer trolleybusNumber;

    @Column(name = "route_id")
    private Integer routeId;

    public Integer getId() {
        return id;
    }

    public void setRouteId(Integer routeId) {
        this.routeId = routeId;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setTrolleybusNumber(Integer trolleybusNumber) {
        this.trolleybusNumber = trolleybusNumber;
    }

    public Integer getRouteId() {
        return routeId;
    }

    public Integer getTrolleybusNumber() {
        return trolleybusNumber;
    }
}
