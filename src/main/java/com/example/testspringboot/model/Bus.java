package com.example.testspringboot.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

    @Entity
    @Table(name = "buses")
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    public class Bus {
        @Id
        @Column(name = "id")
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        private Integer id;

        @Column(name = "bus_number")
        private Integer busNumber;

        @Column(name = "route_id")
        private Integer routeId;

        public Integer getId() {
            return id;
        }

        public Integer getBusNumber() {
            return busNumber;
        }

        public Integer getRouteId() {
            return routeId;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public void setBusNumber(Integer busNumber) {
            this.busNumber = busNumber;
        }

        public void setRouteId(Integer routeId) {
            this.routeId = routeId;
        }
    }
