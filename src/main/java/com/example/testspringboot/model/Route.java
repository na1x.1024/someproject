package com.example.testspringboot.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

@Entity
@Table(name = "routes")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Route {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "initial_stop")
    private String initialStop;

    @Column(name = "ending_stop")
    private String endingStop;

    @Column(name = "route_duration")
    private Double routeDuration;

    public Integer getId() {
        return id;
    }

    public Double getRouteDuration() {
        return routeDuration;
    }

    public String getEndingStop() {
        return endingStop;
    }

    public String getInitialStop() {
        return initialStop;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setEndingStop(String endingStop) {
        this.endingStop = endingStop;
    }

    public void setInitialStop(String initialStop) {
        this.initialStop = initialStop;
    }

    public void setRouteDuration(Double routeDuration) {
        this.routeDuration = routeDuration;
    }
}
