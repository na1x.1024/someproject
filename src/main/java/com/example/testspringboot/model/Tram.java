package com.example.testspringboot.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

@Entity
@Table(name = "trams")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Tram {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "tram_number")
    private Integer tramNumber;

    @Column(name = "route_id")
    private Integer routeId;

    public Integer getId() {
        return id;
    }

    public void setTramNumber(Integer tramNumber) {
        this.tramNumber = tramNumber;
    }

    public Integer getRouteId() {
        return routeId;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getTramNumber() {
        return tramNumber;
    }

    public void setRouteId(Integer routeId) {
        this.routeId = routeId;
    }
}