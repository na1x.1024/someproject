INSERT INTO routes (initial_stop, ending_stop, route_duration) VALUES
    ('guznyanskaya','tsum', 310),
    ('А','B', 190);

INSERT INTO buses (bus_number, route_id) VALUES
    (375, 1),
    (666, 2),
    (137, 2);

INSERT INTO trolleybuses (trolleybus_number, route_id) VALUES
    (263, 2),
    (009, 1);

INSERT INTO trams (tram_number, route_id) VALUES
    (463, 1),
    (867, 1);