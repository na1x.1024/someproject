drop table routes;
drop table trams;
drop table buses;
drop table trolleybuses;


CREATE TABLE IF NOT EXISTS trams
(
    id    BIGSERIAL PRIMARY KEY,
    tram_number VARCHAR NOT NULL,
    route_id INTEGER NOT NULL
);

CREATE TABLE IF NOT EXISTS buses
(
    id    BIGSERIAL PRIMARY KEY,
    bus_number VARCHAR NOT NULL,
    route_id INTEGER NOT NULL
    );

CREATE TABLE IF NOT EXISTS trolleybuses
(
    id    BIGSERIAL PRIMARY KEY,
    trolleybus_number VARCHAR NOT NULL,
    route_id INTEGER NOT NULL
    );

CREATE TABLE IF NOT EXISTS routes
(
    id  BIGSERIAL PRIMARY KEY,
    initial_stop VARCHAR NOT NULL,
    ending_stop VARCHAR NOT NULL,
    route_duration DOUBLE PRECISION NOT NULL
);